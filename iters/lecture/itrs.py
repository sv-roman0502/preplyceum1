import itertools
# from array import array
# print(
#     list(itertools.product(range(1, 11), range(1, 11)))
# )
import functools


def func(x, y):
    x.update(y)
    return x


# ИИН
# print(
#     functools.reduce(lambda x, y: func(x, y), [{'1': 1}, {'2': 2}, {'3': 3}])
# )  # [1, 2, 3, 4, 5, 6]
