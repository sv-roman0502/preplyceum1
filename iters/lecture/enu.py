# filter, map
# array = [8, 9, 120, 670, 1, 2, 3, 4, 5]
# # for i in range(len(array)):  # сделать так, чтобы все четные числа, умножились на 2
# #     if array[i] % 2 == 0:
# #         array[i] = array[i] * 2
# # print(array)
# for idx, value in enumerate(array):
#     if value % 2 == 0:
#         array[idx] = value * 2
# print(*array)
# def is_doubled(first, second):
#     for idx, value in enumerate(first):
#         if value != second[idx] // 2:
#             return False
#     return True
def is_doubled(first, second):
    first = [1, 2, 3, 4, 5]
    second = [2, 4, 6, 8, 10]
    for value1, value2 in zip(first, second):  # (1, 2), (2, 4), (3, 6), (4, 8), (5, 10)
        if value1 != value2 // 2:
            return False
    return True
