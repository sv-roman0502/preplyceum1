# (4->5->6->7->8)
# next

# yield
class It:
    def __init__(self):
        self.arr = list(range(10))  # сами данные, по которым пробегаются

    def __iter__(self):
        self.pointer = 0  # начало
        return self

    def __next__(self):
        self.pointer += 1  # как перейти к след элементу
        if self.pointer > len(self.arr):
            raise StopIteration
        return self.arr[self.pointer - 1]  # что вернуть


for el in It():
    print(el)
