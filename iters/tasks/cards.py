import itertools

skip = input().strip()
cards = ['пик', 'треф', 'бубен', 'червей']
cards.remove(skip)

values = itertools.chain(
    map(str, range(2, 11)), ['валет', 'дама', 'король', 'туз']
)

for value, suit in itertools.product(values, cards):
    print(' '.join([value, suit]))
