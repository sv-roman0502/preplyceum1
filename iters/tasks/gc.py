import sys
import functools
import math


numbers = map(int, sys.stdin)

gcd = functools.reduce(
    lambda res, val: math.gcd(res, val), numbers
)
print(gcd)
