import sys
import functools


value = functools.reduce(
    lambda result, val: result if result <= val else val,
    map(str.strip, sys.stdin)
)
print(value)
