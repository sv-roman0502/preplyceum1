from PIL import Image
import numpy as np


def bw_convert():
    img = np.asarray(Image.open('image.jpg'))
    r, g, b = img[:, :, 0], img[:, :, 1], img[:, :, 2]
    x, y = r.shape
    new_col = (0.2989 * r + 0.5870 * g + 0.1140 * b).reshape(x, y, 1).round().astype(np.uint8)

    img = np.concatenate((new_col, new_col, new_col), axis=2)
    Image.fromarray(img).save('res.jpg')
