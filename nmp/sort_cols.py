import numpy as np


def super_sort(rows, cols):
    a = np.random.randint(1, 101, size=rows * cols).reshape(rows, cols)
    b = a.copy()
    b[:, ::2].sort(axis=0)
    b[:, 1::2][::-1].sort(axis=0)
    return a, b
