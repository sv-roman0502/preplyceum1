import numpy as np

a = [i for i in range(25)]
# b = np.array((1, 2, 3))
# b = np.zeros((2, 3), dtype=np.int64)
# b = np.arange(1, 3, 0.1)
b = np.ones((2, 3), dtype=np.int32)
print(b.sum(axis=1), b)


# [
#     [1, 1, 1],
#     [1, 1, 1]
# ]
# [
#     [2, 2, 2]
# ]

# print(a)
# b = np.arange(30).reshape(5, 6)
# rows, cols = b.shape
# print(b, rows, cols)

#
# [
#     [0, 1, 2, 3, 4],
#     [5, 6, 7, 8, 9],
#     [10, 11, 12, 13, 14],
#
# ]
