import numpy as np

table = np.genfromtxt(
    'ABBREV.csv', delimiter=';', dtype=None,
    names=True, encoding="utf8"
)
table = np.sort(table, order='Energ_Kcal', axis=0)
print(table[-1]['Shrt_Desc'])
table = np.sort(table, order='Sugar_Tot_g', axis=0)
print(table[0]['Shrt_Desc'])
table = np.sort(table, order='Protein_g', axis=0)
print(table[-1]['Shrt_Desc'])
table = np.sort(table, order='Vit_C_mg', axis=0)
print(table[-1]['Shrt_Desc'])
