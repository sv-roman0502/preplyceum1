# шахматная доска

import numpy as np


def make_field(size):
    field = np.zeros(size * size, dtype=np.int8).reshape(size, size)
    field[::2, ::2], field[1::2, 1::2] = 1, 1
    return field
