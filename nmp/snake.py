import numpy as np


def snake(rows, cols):
    matrix = np.arange(1, rows * cols + 1).reshape(rows, cols)
    matrix[1::2, ] = matrix[1::2, ::-1]
    return matrix
