from operator import itemgetter
from types import MappingProxyType

dictionary = MappingProxyType({
    'name': 'roma',
    'age': 22,
    'workplace': 'Halyk Bank'
})
# name, age, work = (dictionary[key] for key in ('name', 'age', 'workplace'))
name, age, *work = itemgetter('name', 'age', 'workplace')(dictionary)
print(name, age, work)
# dictionary[1] = 2
dictionary['name'] = 1
